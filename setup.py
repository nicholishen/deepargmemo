# -*- coding: utf-8 -*-


from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='deepargmemo',
    version='0.0.1',
    description='A lru_cache clone with deep argument memoization',
    long_description=readme,
    author='nicholishen',
    author_email='nicholishen@tutanota.com',
    url='',
    license=license,
    packages=find_packages(exclude=('tests',))
)

