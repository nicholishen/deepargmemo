import time
from functools import lru_cache

from .context import dam_cache


@dam_cache(None)
def foo1(*args, **kwargs):
    time.sleep(.5)
    return True


@dam_cache(None)
def damfunc(*args, **kwargs):
    return True


@lru_cache(None)
def lrufunc(*args, **kwargs):
    return True


def test_time_diff_dam_lru():
    s = time.perf_counter()
    _ = damfunc(1, 2, 3, 4, 5, x=dict(a=6, b=7, c=8, d=9, e=10))
    t1 = time.perf_counter() - s
    s = time.perf_counter()
    _ = lrufunc(1, 2, 3, 4, 5, a=6, b=7, c=8, d=9, e=10)
    t2 = time.perf_counter() - s
    assert abs(t1 - t2) < 0.001 #less than 1 ms


def test_foo1():
    start = time.time()
    for _ in range(1000):
        res = foo1(range, 1, 2, [1, [2, 3]], p={'run': 'run', 3: {1: int}}, g='g')
    assert time.time() - start < 1
    info = foo1.cache_info()
    assert info.misses == 1
    assert info.hits == 999
    assert info.currsize == 1
