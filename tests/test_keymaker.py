from copy import deepcopy

import pytest

from .context import deepargmemo as dam
from .context import make_key


def test_deep_nested_order():
    k1 = make_key(1, 2, k={'a': [1, 2]})
    k2 = make_key(1, 2, k={'a': [2, 1]})
    assert k1 != k2


def test_same_res_from_both_funcs():
    args = (1, 2, 3)
    kwargs = dict(a=1, b=2, c=3)
    k1 = make_key(*args, **kwargs)
    k2 = dam._make_key(args, kwargs)
    assert k1 == k2


def test_common_usage():
    url = "www.foobar.io"
    payload = dict(
        q="Where's the beef?",
        page=7, )
    callback = lambda x: print(x)
    key = make_key(url, params=payload, callback=callback)
    assert hash(key)
    d = {key: 'assert'}
    assert d[key] == 'assert'
    key_mixed = make_key(url, callback=callback, params=payload)
    assert key_mixed == key
    payload = dict(
        page=7,
        q="Where's the beef?",
    )
    key_mixed_payload = make_key(url, callback=callback, params=payload)
    assert key_mixed_payload == key_mixed
    assert d[key_mixed_payload] == 'assert'


def test_no_args_mutated():
    args = (1, 2, 3, [1, 3])
    cargs = deepcopy(args)
    kw = dict(a=1, b={1: 1})
    ckw = deepcopy(kw)
    key = dam.make_key(*args, **kw)
    assert args == cargs and kw == ckw


def test_nesting_args():
    key = dam.make_key([[[[[[1]]]]]])
    assert key == (((((((1,),),),),),),)


def test_nesting_kw():
    key = dam.make_key(x=[[[[[[1]]]]]])
    expected = (frozenset({('x', ((((((1,),),),),),),), }),)
    assert key == expected


def test_hashable():
    key = dam.make_key(1, {1: 1}, [1], {5, 4, 3, 2, 1}, a='apple', b=[[1, [4]]])
    assert hash(key)
    k2 = dam.make_key(1, {1: 1}, [1], {1, 2, 3, 4, 5}, b=[[1, [4]]], a='apple')
    assert k2 == key


def test_unhashable():
    from dataclasses import dataclass
    @dataclass
    class Foo:
        foo: int
        bar: int

    with pytest.raises(TypeError):
        key = dam.make_key(foo={'foo': Foo(1, 2)})


def test_arg_order():
    k1 = dam.make_key(1, 2, 3)
    k2 = dam.make_key(2, 1, 3)
    assert k1 != k2


def test_same_number_of_params():
    k1 = make_key(1, 2, 3)
    k2 = make_key(1, 2, x=3)
    assert k1 != k2


def test_type():
    k1 = dam.make_key(None)
    k2 = dam.make_key("")
    assert k1 != k2


def test_functions():
    k1 = dam.make_key(test_type)
    print(k1)
    assert hash(k1)


def test_nested_dicts():
    x = [
        {'a': 1, 'b': int},
        {'a': 1, 'b': int},
    ]
    k1 = dam.make_key(x)
    assert hash(k1)
    k2 = dam.make_key(x=x)
    assert hash(k2)
    assert k1 != k2


def test_range_constraints():
    args = (range(1000),)
    k1 = dam._deep_key_maker(args)
    assert hash(k1)
    with pytest.raises(ValueError):
        args = (range(1001),)
        k2 = dam._deep_key_maker(args)


def test_iterable_type():
    k1 = dam._deep_key_maker(([1],))
    k2 = dam._deep_key_maker(((1,),))
    assert k1 == k2


if __name__ == '__main__':
    pass
