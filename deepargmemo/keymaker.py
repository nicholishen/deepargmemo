from typing import Generator
from typing import Hashable
from typing import Iterable
from typing import Mapping

LEN_CONSTRAINT = 1000


def make_key(*args, **kwargs):
    """
    This is a helper function which receives actual args and kwargs
    instead of a tuple and a dict.
    :param args:
    :param kwargs:
    :return: Tuple, hashable key of args and kwargs
    """
    return _make_key(args, kwargs)


def _make_key(args, kwds, typed=False):
    """
    Tries to hash with faster lru key-maker first then falls back
    to the _deep_key_maker.
    """
    try:
        return _make_key_original(args, kwds, typed)
    except TypeError:
        if typed == True:
            raise TypeError(
                "decorator 'typed' arg is set to True. I'd like to fallback to deep inspection, but I can't")
        return _deep_key_maker(args, kwds)


class _HashedSeq(list):
    """ This class guarantees that hash() will be called no more than once
        per element.  This is important because the dam_cache() will hash
        the key multiple times on a cache miss.

    """

    __slots__ = 'hashvalue'

    def __init__(self, tup, hash=hash):
        self[:] = tup
        self.hashvalue = hash(tup)

    def __hash__(self):
        return self.hashvalue


def _make_key_original(args, kwds, typed,
                       kwd_mark=(object(),),
                       fasttypes={int, str, frozenset, type(None)},
                       tuple=tuple, type=type, len=len):
    """Make a cache key from optionally typed positional and keyword arguments

    The key is constructed in a way that is flat as possible rather than
    as a nested structure that would take more memory.

    If there is only a single argument and its data type is known to cache
    its hash value, then that argument is returned without a wrapper.  This
    saves space and improves lookup speed.

    """
    # All of code below relies on kwds preserving the order input by the user.
    # Formerly, we sorted() the kwds before looping.  The new way is *much*
    # faster; however, it means that f(x=1, y=2) will now be treated as a
    # distinct call from f(y=2, x=1) which will be cached separately.
    key = args
    if kwds:
        key += kwd_mark
        for item in kwds.items():
            key += item
    if typed:
        key += tuple(type(v) for v in args)
        if kwds:
            key += tuple(type(v) for v in kwds.values())
    elif len(key) == 1 and type(key[0]) in fasttypes:
        return key[0]
    return _HashedSeq(key)


def _deep_key_maker(args=None, kwargs=None):
    """
    Makes a hashable key from deeply nested mutable collections. Positional
    args are respected, and kwargs can be in any order. One minor difference
    in the order of any nested non-hashed types will result in an entirely new key.

    _key_maker scans in iterables recursively until it makes a fully
    hashable key or it runs into an object nested inside that is truly
    unhashable. dicts are converted to frozenset of tuples. sets are
    converted to frozensets. lists are converted to tuples.
    :param args: a non mapping iterable or single item of Hashable nature
    :param kwargs: a mapping
    :return: Tuple, hashable key of args and kwargs
    """
    key = []
    if args:
        for arg in args:
            if isinstance(arg, Iterable) and not isinstance(arg, str):
                if isinstance(arg, set):  # everything in set is already hashed
                    key.append(frozenset(arg))
                elif isinstance(arg, Mapping):
                    key.append(_deep_key_maker(kwargs=arg))
                elif isinstance(arg, Generator):
                    key.append(arg)
                elif hasattr(arg, '__len__'):
                    if len(arg) > LEN_CONSTRAINT:
                        raise ValueError(f'Iterable len too large for key {arg!r}')
                    key.append(_deep_key_maker(args=arg))
                else:
                    raise TypeError(f'{arg!r} is not hashable')
            elif isinstance(arg, Hashable):
                key.append(arg)
            else:
                raise TypeError(f'{arg!r} is not hashable')
    if kwargs:
        kw_set = set()
        for k, v in kwargs.items():
            if isinstance(v, Iterable) and not isinstance(v, str):
                if isinstance(v, set):
                    kw_set.add((k, frozenset(v)))
                elif isinstance(v, Mapping):
                    kw_set.add((k, _deep_key_maker(kwargs=v)))
                elif isinstance(v, Generator):
                    kw_set.add((k, v))
                elif hasattr(v, '__len__'):
                    if len(v) > LEN_CONSTRAINT:
                        raise Exception(f'Iterable len too large for key {v!r}')
                    kw_set.add((k, _deep_key_maker(args=v)))
                else:
                    raise TypeError(f'{v!r} is not hashable')
            elif isinstance(v, Hashable):
                kw_set.add((k, v))
            else:
                raise TypeError(f'{v!r} is not hashable')
        if kw_set:
            key.append(frozenset(kw_set))
    return tuple(key)
